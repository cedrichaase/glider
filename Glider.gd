extends RigidBody

# constants and settings
const CENTER = Vector3(0, 0, 0)

const LIFT = .05
const DRAFT = .25
const THRUST = 180

const MAX_TILT_ANGLE = PI
const TILT_SPEED = 0.01

const HEIGHT_LIMIT = 32
const TERM_VELOCITY = 50

const MAX_SIDE_TRANSLATION = 70

# instance variables
var tilt_z = 0
var tilt_x = 0
var tilt_y = 0
var oldpos=Vector3(0,0,0)

var gamestate = ''
var power = 100.0 # depletable power for manual thrust

func get_world_coords():
	return global_transform.origin

func gas(factor = 1):
	apply_impulse(transform.basis.xform(Vector3(0, 0, 0)), transform.basis.z * THRUST * factor)

func tilt_left():
	tilt_z = max(-MAX_TILT_ANGLE, tilt_z - TILT_SPEED)
func tilt_right():
	tilt_z = min(MAX_TILT_ANGLE, tilt_z + TILT_SPEED)
func tilt_down():
	tilt_x = max(-MAX_TILT_ANGLE, tilt_x - TILT_SPEED)
func tilt_up():
	tilt_x = min(MAX_TILT_ANGLE, tilt_x + TILT_SPEED)

func neutralize_z_tilt():
	var neutralize_speed = TILT_SPEED / 3
	if tilt_z > 0:
		tilt_z -= min(tilt_z, neutralize_speed)
	elif tilt_z < 0:
		tilt_z += max(tilt_z, neutralize_speed)

func neutralize_y_tilt():
	var neutralize_speed = TILT_SPEED
	if tilt_y > 0:
		tilt_y -= min(tilt_y, neutralize_speed)
	elif tilt_y < 0:
		tilt_y += max(tilt_y, neutralize_speed)


func _ready():
	oldpos=get_world_coords()
	game.glider = self
	gas(2500)


var lift
var draft
var thrust
func _integrate_forces(state):
	gas(.06)
	
	if gamestate == 'GameOver':
		get_world_coords().y = game.mapgenerator.get_height(get_world_coords())
		linear_velocity = Vector3(0, 0, 0)
		return

	if game.mapgenerator.has_collided(get_world_coords() + Vector3(0, 5, 0)) and not game.debug_mapgen:
		game.gui.message('You crashed!')
		game.gui.message('You got this far: '+str(Vector3(0,5,0).distance_to(get_world_coords())))
		gamestate = 'GameOver'
		game.gameover()
		
	if game.debug_physics:
		draw_debug()
	
	if game.debug_mapgen:
		set_gravity_scale(0.0)
		$DebugCamera.set_current(true)
		
		var movement = Vector2(0, 0)
		
		if(Input.is_action_pressed("ui_left")):
			movement += Vector2(1, 0)
			
		if(Input.is_action_pressed("ui_right")):
			movement += Vector2(-1, 0)
			
		if(Input.is_action_pressed("ui_up")):
			movement += Vector2(0, 1)
			
		if(Input.is_action_pressed("ui_down")):
			movement += Vector2(0, -1)
		
		apply_central_impulse(Vector3(movement.x, 0, movement.y))
	else:
		# thrust: manual 
		if Input.is_action_pressed("ui_select"):
			if power > 1:
				gas(.4)
				power -= 0.5
		
		if Input.is_action_pressed("ui_left") and get_world_coords().x < MAX_SIDE_TRANSLATION:
			tilt_left()
		
		if Input.is_action_pressed("ui_right") and get_world_coords().x > -MAX_SIDE_TRANSLATION:
			tilt_right()
		
		# Check if glider is too far left or right
		var side_correction = Vector3(0, 0, 0)
		if get_world_coords().x > MAX_SIDE_TRANSLATION or get_world_coords().x < -MAX_SIDE_TRANSLATION:
			linear_velocity.x = 0
			#var diff = get_world_coords().x - MAX_SIDE_TRANSLATION
			#side_correction += Vector3(-diff, 0, 0)
		
		if Input.is_action_pressed("ui_up"):
			tilt_up()
		
		if Input.is_action_pressed("ui_down"):
			tilt_down()
		
		rotate_object_local(Vector3(1, 0, 0), tilt_x)
		rotate_object_local(Vector3(0, 1, 0), tilt_y)
		rotate_object_local(Vector3(0, 0, 1), tilt_z)
		
		neutralize_z_tilt()
		
		var vel = linear_velocity.z
		
		if vel > 0:
			# lift (speed * surface area)
			var height_coefficient = max(0, sin((get_world_coords().y / HEIGHT_LIMIT) + PI/2)) # reduce lift as height increases
			lift = transform.basis.y.normalized() * pow(vel, 2) * LIFT * height_coefficient
			add_force(lift, transform.basis.xform(CENTER))
		
			# draft: opposite to thrust, pow(speed, 2) * n
			var area = Vector3(0, 0, 1).dot(linear_velocity.normalized())
			
			draft = -transform.basis.z.normalized() * pow(vel, 2) * DRAFT * Vector3(.0001, .0001, .1) * area# + side_correction
			add_force(draft, transform.basis.xform(CENTER))

	#look_at(-get_linear_velocity(), Vector3(0, 1, 0))

	if power < 100:
		power += .1

func draw_debug():
	# DEBUG LIFT
	if lift:
		$TriForce/ArrowRed.set_scale(Vector3(1, lift.x, 1))
		$TriForce/ArrowGreen.set_scale(Vector3(1, lift.y, 1))
		$TriForce/ArrowBlue.set_scale(Vector3(1, lift.z, 1))
	# DEBUG draft
	if draft:
		$TriForce/ArrowRed.set_scale(Vector3(1, draft.x, 1))
		$TriForce/ArrowGreen.set_scale(Vector3(1, draft.y, 1))
		$TriForce/ArrowBlue.set_scale(Vector3(1, draft.z, 1))
		
func _process(delta):
	if(oldpos.distance_to(get_world_coords())>1000):
		_struck_by_lightning()
func _struck_by_lightning():
	pass