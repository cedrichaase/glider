extends AudioStreamPlayer

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if not playing:
		if game.mapgenerator.initialized:
			playing = true

	var vel = game.glider.linear_velocity.length()
	set_volume_db(max(1.2 * log(vel) - 8, 0) - 15)
	pitch_scale = max(vel / 50, 1.2)
	
