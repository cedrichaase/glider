extends Node

var mapgenerator
var glider
var gui
var debug_physics = false
var debug_mapgen = false
var time_passed = 0.0
var tree = preload("res://Tree.tscn")
var stone = preload("res://Tree.tscn")
var stone2 = preload("res://Tree.tscn")
var upwind = preload("res://upwind.tscn")

func _ready():
	get_tree().paused = true

func _process(delta):
	time_passed += delta

func gameover():
	get_tree().paused = true
	gui.get_node("PanelMenu").visible = true