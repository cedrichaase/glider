extends Spatial

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var texture=ImageTexture.new()
		
var heightmaps=[]#heightmaps in order of creation
var heightmapcoords=[]#coordinate pairs in order of creation
var edges
"""var noisefactort=[]
var noisefactorl=[]
var noisefactorb=[]
var noisefactorr=[]"""
var blocksize=64
const RADIUS = 2
var generated_chunks = []

var mapgen_thread = Thread.new()

var initialized = false

# Member variables
var thread = Thread.new()

# This function runs in a thread!
func _sync_with_map(useful_argument):
	while true:
		if not game.glider:
			continue
		var glider_coords = game.glider.get_world_coords()
		var glider_block_coords = world_coords_to_block(glider_coords)
		generate_dims(glider_block_coords, 1, 6)


# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	texture.load(str("assets/textures/map.png"))
	texture.set_flags(5)
	#_generate_map(-1,-1)
	#_generate_map(-1,0)
	#_generate_map(0,0)
	#_generate_map(0,-1)
	game.mapgenerator = self
	generate_dims(Vector2(0, 0), 1, 6)
	if thread.is_active():
		# Already working
		return
	thread.start(self, "_sync_with_map")
	initialized = true
	
	
	pass # Replace with function body.
	
func has_collided(var pos):
	if get_height(pos)>pos.y:
		return true
	else:
		return false

func get_height(var pos):
	var heightmap_xcoord=int(floor(pos.x/float(blocksize)))
	var heightmap_ycoord=int(floor(pos.z/float(blocksize)))
	var currentheightmap=heightmapcoords.find([heightmap_xcoord,-heightmap_ycoord])
	if(currentheightmap!=-1):
		var blsquare_x=int(floor(pos.x-heightmap_xcoord*blocksize))
		var blsquare_y=int(floor(pos.z-heightmap_ycoord*blocksize))
		var restx=pos.x-heightmap_xcoord*blocksize-blsquare_x
		var resty=pos.z-heightmap_ycoord*blocksize-blsquare_y
		var height=0.0
		if((restx+resty)<1):
			height=heightmaps[currentheightmap][blsquare_x][blsquare_y]*(1-(restx+resty))
			var diffx=heightmaps[currentheightmap][blsquare_x+1][blsquare_y]-height
			var diffy=heightmaps[currentheightmap][blsquare_x][blsquare_y+1]-height
			height+=diffx*restx
			height+=diffy*resty
		else:
			height=heightmaps[currentheightmap][blsquare_x+1][blsquare_y+1]
			var diffx=height-heightmaps[currentheightmap][blsquare_x+1][blsquare_y]
			var diffy=height-heightmaps[currentheightmap][blsquare_x][blsquare_y+1]
			height+=diffx*restx
			height+=diffy*resty
		return height
	return - 6.0
	pass
"""func _generate_noisefactors():
	for i in range(blocksize+1):
		noisefactorb.push_back([])
		noisefactort.push_back([])
		noisefactorl.push_back([])
		noisefactorr.push_back([])
		for j in range(blocksize+1):
			noisefactorb[i].push_back(max(0,1-float(j)/(blocksize/2)))
			noisefactort[i].push_back(max(0,float(j)/(blocksize/2))-1)
			noisefactorl[i].push_back(max(0,1-float(i)/(blocksize/2)))
			noisefactorr[i].push_back(max(0,float(i)/(blocksize/2))-1)
			var noisefactorl_tmp=noisefactorl[i][j]
			var noisefactorr_tmp=noisefactorr[i][j]
			noisefactorl[i][j]*=1.0-noisefactorb[i][j]
			noisefactorl[i][j]*=1.0-noisefactort[i][j]
			noisefactorr[i][j]*=1.0-noisefactorb[i][j]
			noisefactorr[i][j]*=1.0-noisefactort[i][j]
			noisefactorb[i][j]*=1.0-noisefactorl_tmp
			noisefactorb[i][j]*=1.0-noisefactorr_tmp
			noisefactort[i][j]*=1.0-noisefactorl_tmp
			noisefactort[i][j]*=1.0-noisefactorr_tmp
"""
func _generate_map(var x,var y):#generates cluster of size blocksize*blocksize at coordinates x*blocksize,y*blocksize in size 1 squares
	if(heightmapcoords.find([x,y])==-1):
		var leftborder=[]
		leftborder.push_back([])
		var arraynrl=heightmapcoords.find([x-1,y])
		if(arraynrl!=-1):
			#print("found cluster left of this one")
			leftborder[0]=heightmaps[arraynrl][64]
		var rightborder=[]
		rightborder.push_back([])
		var arraynrr=heightmapcoords.find([x+1,y])
		if(arraynrr!=-1):
			#print("found cluster right of this one")
			rightborder[0]=heightmaps[arraynrr][0]
		var topborder=[]
		var arraynrt=heightmapcoords.find([x,y+1])
		if(arraynrt!=-1):
			#print("found cluster above this one")
			for column in heightmaps[arraynrt]:
				topborder.push_back([])
				topborder[topborder.size()-1].push_back(column[0])
		var bottomborder=[]
		var arraynrb=heightmapcoords.find([x,y-1])
		if(arraynrb!=-1):
			#print("found cluster below this one")
			for column in heightmaps[arraynrb]:
				bottomborder.push_back([])
				bottomborder[bottomborder.size()-1].push_back(column[64])
		var count=0
		"""var val=0.0
		for value in rightborder[0]:
			count+=1
			val+=value
		for value in leftborder[0]:
			count+=1
			val+=value 
		for value in bottomborder:
			count+=1
			val+=value[0]
		for value in topborder:
			count+=1
			val+=value[0]
		if count>0:
			val/=float(count)"""
		if(leftborder[0].empty()):
			if(rightborder[0].empty()||!topborder.empty()||!bottomborder.empty()):
				if topborder.empty():
					if bottomborder.empty():
						leftborder[0].push_back(0)
					else:
						leftborder[0].push_back(bottomborder[0][0])
					for i in range(blocksize):
						leftborder[0].push_back((randf()*2-1)+leftborder[0][i])
				else:
					if bottomborder.empty():
						leftborder[0].push_front(topborder[0][0])
						for i in range(blocksize):
							leftborder[0].push_front((randf()*2-1)+leftborder[0][0])
					else:#top and bottom exist
						var frombottom=[bottomborder[0][0]]
						for i in range(blocksize):
							frombottom.push_back(frombottom[i]+(randf()*2-1))
						leftborder[0].push_front(topborder[0][0])
						for i in range(blocksize):
							leftborder[0].push_front((leftborder[0][0]+(randf()*2-1))*(float(blocksize-i-1)/float(blocksize))+frombottom[blocksize-i]*float(i)/float(blocksize-1))
			else:#rightborder is not empty and topborder and bottomborder are empty
				bottomborder.push_front([rightborder[0][0]])
				for i in range(blocksize):
					bottomborder.push_front([])
					bottomborder[0].push_front((randf()*2-1)+bottomborder[1][0])
				leftborder[0].push_back(bottomborder[0][0])
				for i in range(blocksize):
					leftborder[0].push_back((randf()*2-1)+leftborder[0][i])
		if(topborder.empty()):
			topborder.push_front([leftborder[0][blocksize]])
			if(rightborder[0].empty()):
				for i in range(1,blocksize+1):
					topborder.push_back([])
					topborder[i].push_back((randf()*2-1)+topborder[i-1][0])
			else:
				var fromright=[rightborder[0][blocksize]]
				for i in range(blocksize):
					fromright.push_front((randf()*2-1)+fromright[0])
				for i in range(blocksize):
					topborder.push_back([])
					topborder[topborder.size()-1].push_back((topborder[i][0]+(randf()*2-1))*(float(blocksize-1-i)/float(blocksize-1))+fromright[i+1]*float(i)/float(blocksize-1))
		if(bottomborder.empty()):
			bottomborder.push_back([leftborder[0][0]])
			if(rightborder[0].empty()):
				for i in range(1,blocksize+1):
					bottomborder.push_back([])
					bottomborder[i].push_back((randf()*2-1)+bottomborder[i-1][0])
			else:
				var fromright=[rightborder[0][0]]
				for i in range(blocksize+1):
					fromright.push_front((randf()*2-1)+fromright[0])
				for i in range(1,blocksize+1):
					bottomborder.push_back([])
					bottomborder[i].push_back((bottomborder[i-1][0]+(randf()*2-1))*(float(blocksize-i-1)/float(blocksize))+fromright[i+1]*float(i)/float(blocksize-1))
		if(rightborder[0].empty()):
			rightborder[0].push_front(topborder[blocksize][0])
			var frombottom=[bottomborder[blocksize][0]]
			for i in range(blocksize):
				frombottom.push_back(frombottom[i]+(randf()*2-1))
			for i in range(1,blocksize+1):
				var toadd = (rightborder[0][0]+(randf()*2-1))*(float(blocksize-i)/float(blocksize))
				toadd+=frombottom[blocksize-i]*float(i)/float(blocksize)
				rightborder[0].push_front(toadd)
		#print("bottom")
		#print(bottomborder)
		#print("left")
		#print(leftborder)
		#print("right")
		#print(rightborder)
		#print("top")
		#print(rightborder)
		for i in range(1,blocksize+1):
			leftborder.push_back([])
			rightborder.push_front([])
			for j in range(blocksize+1):
				bottomborder[j].push_back((randf()*2-1)+bottomborder[j][i-1])
				leftborder[i].push_back((randf()*2-1)+leftborder[i-1][j])
				rightborder[0].push_front((randf()*2-1)+rightborder[1][j])
				topborder[j].push_front((randf()*2-1)+topborder[j][0])
				
		var heightmap= []
		for i in range(blocksize+1):
			heightmap.push_back([])
			for j in range(blocksize+1):
				var bottom=max(0,1-float(j)/(blocksize/2))
				var top=max(0,float(j)/(blocksize/2)-1)
				var left=max(0,1-float(i)/(blocksize/2))
				var right=max(0,float(i)/(blocksize/2)-1)
				if(left==1):
					heightmap[i].push_back(leftborder[i][j])
				elif(bottom==1):
					heightmap[i].push_back(bottomborder[i][j])
				elif(top==1):
					heightmap[i].push_back(topborder[i][j])
				elif(right==1):
					heightmap[i].push_back(rightborder[i][j])
				else:
					var factor=bottom+left+right+top
					var lostnoise= max(0,1-factor)
					var topval=topborder[i][blocksize]
					var bottomval=bottomborder[i][0]
					var rightval=rightborder[blocksize][j]
					var leftval=leftborder[0][j]
					var middle_val=(bottomval*(blocksize-j)/float(blocksize)+
						topval*j/float(blocksize)+
						leftval*(blocksize-i)/float(blocksize)+
						rightval*i/float(blocksize))/2
					if((i==63)&&j==63):
						#print("test2")
						pass
					heightmap[i].push_back(max(middle_val+(randf()*2-1)*lostnoise+(
						(middle_val-leftborder[i][j])*left/max(1,factor)+
						(middle_val-bottomborder[i][j])*bottom/max(1,factor)+
						(middle_val-topborder[i][j])*top/max(1,factor)+
						(middle_val-rightborder[i][j])*right/max(1,factor)),-60))
				#var lostnoise=max(0,1-(noisefactorl[i][j]+noisefactorb[i][j]+noisefactorr[i][j]+noisefactort[i][j]))
				#print(lostnoise)
				#heightmap[i].push_back(*noisefactorl[i][j]+rightborder[blocksize-i][j]*noisefactorr[i][j]+bottomborder[j][i]*noisefactorb[i][j]+topborder[blocksize-j][i]*noisefactort[i][j]+(randf()*2-1)*lostnoise)
		for i in range(1,blocksize):
			for j in range(1,blocksize):
				heightmap[i][j]=(heightmap[i-1][j-1]+heightmap[i-1][j]+heightmap[i-1][j+1]+heightmap[i][j-1]+heightmap[i][j]+heightmap[i][j+1]+heightmap[i+1][j]+heightmap[i+1][j-1]+heightmap[i+1][j+1])/9
		var Map = Mesh.new()
		var Mapinstance = MeshInstance.new()
		var st = SurfaceTool.new()
		var vertices = PoolVector3Array()
		var UVs = PoolVector2Array()
		var Treecoords = []
		var Stonecoords = []
		var color = Color(0.1, 0.9, 0.1)
		var mat = SpatialMaterial.new()
		mat.albedo_texture=texture
		mat.albedo_color = Color(1,1,1)
		st.begin(Mesh.PRIMITIVE_TRIANGLES)
		st.set_material(mat)
		for i in range(len(heightmap)-1):
			for j in range(len(heightmap[i])-1):
				var canbetree=false
				if(heightmap[i][j]<-6 and heightmap[i][j+1]<-6 and heightmap[i+1][j+1]<-6):
					UVs.push_back(Vector2(0.25,0))
					UVs.push_back(Vector2(0.25,0.5))
					UVs.push_back(Vector2(0.5,0.5))
				elif(abs(heightmap[i][j]-heightmap[i][j+1])>0.6 or abs(heightmap[i][j+1]-heightmap[i+1][j+1])>0.6):
					Stonecoords.push_back(Vector3(i,heightmap[i][j]+1,-j))
					UVs.push_back(Vector2(0,0))
					UVs.push_back(Vector2(0,0.5))
					UVs.push_back(Vector2(0.25,0.5))
				elif(heightmap[i][j]>6 and heightmap[i][j+1]>6 and heightmap[i+1][j+1]>6):
					UVs.push_back(Vector2(0,0.5))
					UVs.push_back(Vector2(0,1))
					UVs.push_back(Vector2(0.25,1))
				elif(abs(heightmap[i][j]-heightmap[i][j+1])<0.2 and abs(heightmap[i][j+1]-heightmap[i+1][j+1])<0.2):
					UVs.push_back(Vector2(0.5,0))
					UVs.push_back(Vector2(0.5,0.5))
					UVs.push_back(Vector2(0.75,0.5))
				elif(abs(heightmap[i][j]-heightmap[i][j+1])<0.3 and abs(heightmap[i][j+1]-heightmap[i+1][j+1])<0.3):
					UVs.push_back(Vector2(0.75,0))
					UVs.push_back(Vector2(0.75,0.5))
					UVs.push_back(Vector2(1,0.5))
				elif(abs(heightmap[i][j]-heightmap[i][j+1])<0.4 and abs(heightmap[i][j+1]-heightmap[i+1][j+1])<0.4):
					UVs.push_back(Vector2(0.75,0.5))
					UVs.push_back(Vector2(0.75,1))
					UVs.push_back(Vector2(1,1))
				elif(heightmap[i][j]<-4 and heightmap[i][j+1]<-4 and heightmap[i+1][j+1]<-4):
					UVs.push_back(Vector2(0.5,0.5))
					UVs.push_back(Vector2(0.5,1))
					UVs.push_back(Vector2(0.75,1))
				else:
					canbetree=true
					UVs.push_back(Vector2(0.25,0.5))
					UVs.push_back(Vector2(0.25,1))
					UVs.push_back(Vector2(0.5,1))
				if(heightmap[i][j]<-6 and heightmap[i+1][j]<-6 and heightmap[i+1][j+1]<-6):
					UVs.push_back(Vector2(0.25,0.5))
					UVs.push_back(Vector2(0.5,0.5))
					UVs.push_back(Vector2(0.5,0))
				elif(abs(heightmap[i][j]-heightmap[i+1][j])>0.6 or abs(heightmap[i+1][j]-heightmap[i+1][j+1])>0.6):
					UVs.push_back(Vector2(0.25,0.5))
					UVs.push_back(Vector2(0.25,0))
					UVs.push_back(Vector2(0,0))
					Stonecoords.push_back(Vector3(i,heightmap[i][j]+1,-j))
				elif(heightmap[i][j]>6 and heightmap[i+1][j]>6 and heightmap[i+1][j+1]>6):
					UVs.push_back(Vector2(0,0.5))
					UVs.push_back(Vector2(0.25,1))
					UVs.push_back(Vector2(0.25,0.5))
				elif(abs(heightmap[i][j]-heightmap[i+1][j])<0.2 and abs(heightmap[i+1][j]-heightmap[i+1][j+1])<0.2):
					UVs.push_back(Vector2(0.5,0))
					UVs.push_back(Vector2(0.5,0.5))
					UVs.push_back(Vector2(0.75,0.5))
				elif(abs(heightmap[i][j]-heightmap[i+1][j])<0.3 and abs(heightmap[i+1][j]-heightmap[i+1][j+1])<0.3):
					UVs.push_back(Vector2(0.75,0))
					UVs.push_back(Vector2(0.75,0.5))
					UVs.push_back(Vector2(1,0.5))
				elif(abs(heightmap[i][j]-heightmap[i+1][j])<0.4 and abs(heightmap[i+1][j]-heightmap[i+1][j+1])<0.4):
					UVs.push_back(Vector2(0.75,0.5))
					UVs.push_back(Vector2(0.75,1))
					UVs.push_back(Vector2(1,1))
				elif(heightmap[i][j]<-4 and heightmap[i+1][j]<-4 and heightmap[i+1][j+1]<-4):
					UVs.push_back(Vector2(0.5,0.5))
					UVs.push_back(Vector2(0.5,1))
					UVs.push_back(Vector2(0.75,1))
				else:
					if canbetree:
						Treecoords.push_back(Vector3(i,heightmap[i][j],-j))
					UVs.push_back(Vector2(0.25,0.5))
					UVs.push_back(Vector2(0.5,1))
					UVs.push_back(Vector2(0.5,0.5))
				vertices.push_back(Vector3(i,heightmap[i][j],-j))
				vertices.push_back(Vector3(i,heightmap[i][j+1],-(j+1)))
				vertices.push_back(Vector3(i+1,heightmap[i+1][j+1],-(j+1)))
				vertices.push_back(Vector3(i+1,heightmap[i+1][j+1],-(j+1)))
				vertices.push_back(Vector3(i+1,heightmap[i+1][j],-j))
				vertices.push_back(Vector3(i,heightmap[i][j],-j))
		for i in vertices.size():
			st.add_color(color)
			st.add_uv(UVs[i])
			st.add_vertex(vertices[i])
			#st.add_color(Color(0,0,1))
			#st.add_color(Color(max(0,min(1,vertices[i].z/10)),0.9,max(0,min(1,vertices[i].z/10))))
		st.generate_normals()
		st.commit(Map)
		#$Spatial/MeshInstance.mesh=Map
		Mapinstance.mesh=Map
		Mapinstance.name="Map_"+str(x)+"_"+str(y)
		$Map.add_child(Mapinstance)
		var difference=Vector3(blocksize*x,0,-blocksize*y)
		Mapinstance.translate(difference)
		heightmaps.push_back(heightmap)
		heightmapcoords.push_back([x,y])
		var treecount=10
		for i in range(treecount):
			if(i>Treecoords.size()-2):
				break
			var trycoord =randi()%(Treecoords.size()-1)
			#if(Treecoords.find(Treecoords[trycoord]-Vector3(-1,0,0))!=-1 or Treecoords.find(Treecoords[trycoord]-Vector3(1,0,0))!=-1 or Treecoords.find(Treecoords[trycoord]-Vector3(0,0,-1))!=-1 or Treecoords.find(Treecoords[trycoord]-Vector3(0,0,1))!=-1):
			var pos3D=Treecoords[trycoord]
			#pos3D+=difference
			Treecoords.remove(trycoord)
			var tree=game.tree.instance()
			Mapinstance.add_child(tree)
			tree.translate(pos3D)
			if(randf()<(1.0/20.0)):
				var upwind=game.upwind.instance()
				var scale = 0.5
				upwind.set_scale(Vector3(scale, scale, scale))
				tree.add_child(upwind)
		var stonecount=30
		for i in range(stonecount):
			if(i>Stonecoords.size()-1):
				break
			var trycoord =randi()%(Stonecoords.size()-1)
			#if(Treecoords.find(Treecoords[trycoord]-Vector3(-1,0,0))!=-1 or Treecoords.find(Treecoords[trycoord]-Vector3(1,0,0))!=-1 or Treecoords.find(Treecoords[trycoord]-Vector3(0,0,-1))!=-1 or Treecoords.find(Treecoords[trycoord]-Vector3(0,0,1))!=-1):
			var pos3D=Stonecoords[trycoord]
	#		pos3D+=difference
			Stonecoords.remove(trycoord)
			var stone
			if(randf()>0.5):
				stone=game.stone2.instance()
			else:
				stone=game.stone.instance()
			Mapinstance.add_child(stone)
			stone.translate(pos3D)
	#	pass
	pass

func generate_surrounding(x, y):
	if not generated_chunks.has(Vector2(x, y)):
		generated_chunks.push_back(Vector2(x, y))
		_generate_map(x, y)

func generate_surrounding_range(coords, r):
	var x = coords.x
	var y = coords.y

	for a in range(x-r, x+r):
		for b in range(y-r, y+r):
			generate_surrounding(a, b)

func generate_dims(coords, r, w):
	var x = coords.x
	var y = coords.y

	for a in range(x-r, x+r):
		for b in range(y-w, y+w):
			generate_surrounding(a, b)

func world_coords_to_block(world_coords):
	var map_position = world_coords / float(blocksize)
	return Vector2(floor(map_position.x), floor(-map_position.z))

func reset():
	heightmaps.clear()
	heightmapcoords.clear()
	generated_chunks.clear()
	for child in $Map.get_children():
		child.free()
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
