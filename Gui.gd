extends CanvasLayer


func _ready():
	game.gui = self
	message("There's a glider on the storm!")


func update_gui():
	var altitude = floor(game.glider.transform.origin.y*100)/100
	var time_passed = floor(game.time_passed*100)/100
	$PanelUpperLeft/VBoxContainer/HBoxAltitude/LabelAltitudeValue.set_text(str(altitude))
	$PanelUpperLeft/VBoxContainer/HBoxTime/LabelTimeValue.set_text(str(time_passed))
	if game.glider and game.gui:
		$PanelThrust/ProgressBar.value = game.glider.power

func message(msg):
	$PanelBottomLeft/RichTextLabel.add_text(str(msg) + "\n")

func _on_Timer_timeout():
	update_gui()


func _on_ButtonStart_pressed():
	print("pressed")
	get_tree().paused = false
	$PanelMenu.visible = false

func _on_ButtonExit_pressed():
	get_tree().quit()


func _on_ButtonRestart_pressed():
	game.mapgenerator.reset()
	game.glider.reset()
	game.time_passed=0.0
	pass # Replace with function body.
