extends KinematicBody


const ROTATION_SPEED = 1


var speed = 20.0
var target_rotation_z = 0.0
var target_rotation_x = 0.0
var xrotation=0.0
var zrotation=0.0


func _ready():
	game.glider = self


func _physics_process(delta):
	
	target_rotation_z = 0.0
	target_rotation_x = 0.0
	if (Input.is_action_pressed("ui_left")):
		target_rotation_z = -45.0
	if (Input.is_action_pressed("ui_right")):
		target_rotation_z = 45.0
	if (Input.is_action_pressed("ui_up")):
		target_rotation_x = -45.0
	if (Input.is_action_pressed("ui_down")):
		target_rotation_x = 45.0
	rotation=Vector3(0,0,0)
	if (zrotation != target_rotation_z):
		if target_rotation_z >zrotation:
			zrotation += min(ROTATION_SPEED * delta,target_rotation_z-zrotation)
		elif target_rotation_z<zrotation:
			zrotation -= min(ROTATION_SPEED * delta,zrotation-target_rotation_z)
	rotation.z=zrotation
	
	#var torotatex=0
	if (xrotation != target_rotation_x):
		if target_rotation_x > xrotation:
			xrotation += min(ROTATION_SPEED * delta,target_rotation_x-xrotation)
		elif target_rotation_x<xrotation:
			xrotation -= min(ROTATION_SPEED * delta,xrotation-target_rotation_x)
		rotate_object_local(Vector3(1,0,0),xrotation)
	#$Camera.rotation.x=0-xrotation/2
	speed-=delta*0.3+delta*0.3*min(xrotation,0)-delta*0.3*min(-xrotation,0)
	translate_object_local(Vector3(0, 0, speed) * delta)#flyforwards
	translate_object_local(Vector3(0,0.2*delta,0))#lift
	global_translate(Vector3(0,-delta,0))#gravity